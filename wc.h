/////////////////////////////////////////////////////////////////////////////////
//           University of Hawaii, College of Engineering
/// @brief   Lab01d - wc - EE 491 - Spr 2022
///
/// @file    wc.c
/// @version 2.0 - Updated version
///
/// wc - print newline, word, and byte counts for each file
///
/// @author Jake Imanaka   <jimanaka@hawaii.edu>
/// @date   21_01_2022
///
/// @see     https://linuxcommand.org/lc3_man_pages/wc1.html
///
///////////////////////////////////////////////////////////////////////////////
#ifndef WC_FUNCTIONS_H
#define WC_FUNCTIONS_H 

// program name
#define PROGRAM "wc"

// parses command line inputs for file names and command line options.
int command_line_parse(int argc, char** argv, char** filenames, uint8_t* optionFlags);

// counts the words, bytes, and lines for a file or stdin.
void do_wc_on_file(FILE* fp, int* lines, int* words, int* chars);


#endif
