###############################################################################
###          University of Hawaii, College of Engineering
### @brief   Lab 01d - wc - EE 205 - Spr 2022
###
### @file    Makefile
### @version 2.0 - Updated version
###
### Build a Word Counting program
###
### @author  Jake Imanaka	<jimanaka@hawaii.edu>
### @date    22_01_2021
###
### @see     https://www.gnu.org/software/make/manual/make.html
###############################################################################

CC     = gcc
CFLAGS = -g -Wall
TARGET = wc
OBJFILES = wc.o command-line-parser.o do-wc-on-file.o
DEPENDENCIES = wc.h 

all: $(TARGET)

$(TARGET): $(OBJFILES)
	$(CC) -o $(TARGET) $(OBJFILES)

wc.o: wc.c $(DEPENDENCIES)
	$(CC) $(CFLAGS) -c -o wc.o wc.c

command-line-parser.o: command-line-parser.c $(DEPENDENCIES)
	$(CC) $(CFLAGS) -c -o command-line-parser.o command-line-parser.c

do-wc-on-file.o: do-wc-on-file.c $(DEPENDENCIES)
	$(CC) $(CFLAGS) -c -o do-wc-on-file.o do-wc-on-file.c

test:
	./maketest.sh

clean:
	rm -f $(TARGET) *.o

