/////////////////////////////////////////////////////////////////////////////////
//           University of Hawaii, College of Engineering
/// @brief   Lab01d - wc - EE 491 - Spr 2022
///
/// @file    wc.c
/// @version 2.0 - Updated version
///
/// wc - print newline, word, and byte counts for each file
///
/// @author Jake Imanaka   <jimanaka@hawaii.edu>
/// @date   21_01_2022
///
/// @see     https://linuxcommand.org/lc3_man_pages/wc1.html
///
///////////////////////////////////////////////////////////////////////////////
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <stdint.h>
#include "wc.h"

// counts lines, words, and chars for a given file
void do_wc_on_file(FILE* fp, int* lines, int* words, int* chars) {
    
   // represents when the state machine is currently in a word or outside of a word
   enum inWord{in, out} positionState = out;

   // different supported text encodings
   enum encodingTypes{ASCII, UnicodeBE, UnicodeLE} encoding = ASCII;

   // list of printable characters that can count as words
   static char* printableChars = "abcdefghijklmnopqrstuvwxyz1234567890-=!@#$%^&*()_+[]{};\':\",./<>?\\`~";

   char buffer = 0;           // char buffer to store extra data from unicdoe
   char readchar = 0;         // char read from file
   char lastnewline = '\n';   // keeps track of the last newline char.  used for carriage returns \r\n but it may not be needed.
   int filesize = 0;          // size of the file being read

   // if we are reading a file
   if(fp != stdin){

      // get file size
      fseek(fp, 0l, SEEK_END);
      filesize = ftell(fp);
      rewind(fp);

      // if file has at least two bytes, read the first two bytes and check if it is a unicode encoding
      if(filesize >= 2){
         buffer = fgetc(fp);
         readchar = fgetc(fp);
         if((unsigned char)buffer == 0xFE && (unsigned char)readchar == 0xFF) encoding = UnicodeBE;
         else if((unsigned char)buffer == 0xFF && (unsigned char)readchar == 0xFE) encoding = UnicodeLE;
         rewind(fp);
      }  

      // if the file is a Unicode file, ignore 2 or 3 bytes but count them as chars
      // @todo replace fgetc's with fread
      if(encoding == UnicodeLE){
         fgetc(fp);
         fgetc(fp);
         *chars += 2;
      } else if(encoding == UnicodeBE) {   //ignore first 3 bytes if Unicode Big Endian
         fgetc(fp);
         fgetc(fp);
         fgetc(fp);
         *chars += 3;
      }
   }


   // state machine that reads file char by char
   while((readchar = fgetc(fp)) != EOF)
   {
      // increment char counter
      (*chars)++;

      // if the char is a printable char
      if(strchr(printableChars, tolower(readchar)) != NULL && readchar != '\0'){

         // if we are out of a word, change to being in a word
         if(positionState == out){
            positionState = in;
            // increment word counter
            (*words)++;
         } else {
            //do nothing
         }
      } else {
         // else the char is not a printable char

         // if we were in a word, change state to be out of word
         if(positionState == in) positionState = out;

         // checking for different types of newlines
         // this machien counts '\r' as a newline in an attempt to support Mac's carriage newlines
         // @todo: make sure '\r' alone does not count on other files.
         if(readchar == '\r'){
            (*lines)++;
            lastnewline = '\r';
         } else if(readchar == '\n') {

            // meant to avoid double counting the /r/n newline used on some systems
            if(lastnewline != '\r') (*lines)++;
            lastnewline = '\n';
         }
      }
      
      // if the encoding is unicode, skip the next byte
      if(encoding == UnicodeBE || encoding == UnicodeLE){
         buffer = fgetc(fp);
         (*chars)++;
      }

   }

}
