/////////////////////////////////////////////////////////////////////////////////
//           University of Hawaii, College of Engineering
/// @brief   Lab01d - wc - EE 491 - Spr 2022
///
/// @file    wc.c
/// @version 2.0 - Updated version
///
/// wc - print newline, word, and byte counts for each file
///
/// @author Jake Imanaka   <jimanaka@hawaii.edu>
/// @date   21_01_2022
///
/// @see     https://linuxcommand.org/lc3_man_pages/wc1.html
///
///////////////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdint.h>
#include "wc.h"

// parses command line for filenames and command line options
int command_line_parse(int argc, char** argv, char** filenames, uint8_t* optionFlags) {

   // optionFlags bit settings
   // 0001 bytes
   // 0010 lines
   // 0100 words
   // 1000 version

   // fileCount is the number of filenames
   // stringOptionsCount is the number of options with a '--' prefix, 
   // charOptionCount is number of single char options with a '-' prefix
   int fileCount = 0, stringOptionsCount = 0, charOptionsCount = 0;

   char charOptions[100];        // stores char options
   bzero(charOptions, sizeof(charOptions));
   char* stringOptions[argc];    // stores string options

   // used to cat char options to charOptions
   char catchar[2];
   catchar[0] = '\0';
   catchar[1] = '\0';

   // loop through each command line argument starting after the program name
   for(int i = 1; i < argc; i++)
   {
      // if the argument does not start with '-', it is a filename
      if(argv[i][0] != '-') 
      {
         // adding filename to filenames array
         filenames[fileCount] = (char*)malloc(strlen(argv[i]));
         memcpy(filenames[fileCount], argv[i],strlen(argv[i]));
         fileCount++;
      } else {
         // else the argument does start with '-'; it is a command-line option

         // fail if no option was given
         if(strlen(argv[i]) < 2) {
            fprintf(stderr, "%s: invalid option\n", PROGRAM);
            exit(EXIT_FAILURE);
         } 

         // if the second character is a '-', it is a string option (e.g. --version, --words, etc.)
         if(argv[i][1] == '-') {
            // adding the string option to the stringOptions array
            stringOptions[stringOptionsCount] = (char*)malloc(strlen(argv[i]));
            memcpy(stringOptions[stringOptionsCount], argv[i]+2, strlen(argv[i])  - 2);
            stringOptionsCount++;
         } else {
            // else the option is a char option (e.g. -c, -w, etc.)

            // concat each option to charOptions
            for(int j = 1; j < strlen(argv[i]); j++)
            {
               catchar[0] = argv[i][j];
               strcat(charOptions, catchar);
            }
         }
      }
   }
   
   strcat(charOptions, "\0");
   // getting number of char options
   charOptionsCount = strlen(charOptions);
   
   // if no charOptions or stringOptions, use default (0x0111 to print words, lines, and chars)
   if(charOptionsCount == 0 && stringOptionsCount == 0){
      *optionFlags = 7;
   } else {
      // else there are charOptions or stringOptions

      // check for each type of char option and set optionFlags accordingly
      for(int i = 0; i < charOptionsCount; i++)
      {
         if(charOptions[i] == 'c') *optionFlags = *optionFlags | 1;                // (0x0001)
         else if(charOptions[i] == 'l') *optionFlags = *optionFlags | (1 << 1);    // (0x0010)
         else if(charOptions[i] == 'w') *optionFlags = *optionFlags | (1 << 2 );    // (0x0100)
         else {
            fprintf(stderr, "%s: invalid option -- \'%c\'\n", PROGRAM, charOptions[i]);
            exit(EXIT_FAILURE);
         }
      }

      // check for each type of string option and set optionFlags accordingly
      for(int i = 0; i < stringOptionsCount; i++)
      {
         if(strcmp(stringOptions[i], "bytes") == 0) *optionFlags = *optionFlags | 1;               // (0x0001)
         else if(strcmp(stringOptions[i], "lines") == 0) *optionFlags = *optionFlags | (1 << 1);     // (0x0010)  
         else if(strcmp(stringOptions[i], "words") == 0) *optionFlags = *optionFlags | (1 << 2);     // (0x0100)
         else if(strcmp(stringOptions[i], "version") == 0) *optionFlags = *optionFlags | (1 << 3);   // (0x1000)
         else {
            fprintf(stderr, "%s: invalid option -- \'%s\'\n", PROGRAM, stringOptions[i]);
            exit(EXIT_FAILURE);
         }
      }
   }

   // free the 2d stringOptions array
   for(int i = 0; i < stringOptionsCount; i++)
   {
      free(stringOptions[i]);
   }

   return fileCount;
}
