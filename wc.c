/////////////////////////////////////////////////////////////////////////////////
//           University of Hawaii, College of Engineering
/// @brief   Lab01d - wc - EE 491 - Spr 2022
///
/// @file    wc.c
/// @version 2.0 - Updated version
///
/// wc - print newline, word, and byte counts for each file
///
/// @author Jake Imanaka   <jimanaka@hawaii.edu>
/// @date   21_01_2022
///
/// @see     https://linuxcommand.org/lc3_man_pages/wc1.html
///
///////////////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <stdint.h>
#include "wc.h"

#define VERSION 2
// prints number words, lines, and chars depending on the bits set in optionFlags
void print_output_with_options(int lines, int words, int chars, char* filename, uint8_t optionFlags) {
   
   // optionFlags settings
   // 0001 (1) print chars
   // 0010 (1 << 1) print lines
   // 0100 (1 << 2) print words

   // check if lines flag is set
   if((optionFlags & (1 << 1)) == ( 1 << 1)) printf("%d\t", lines);

   // check if words flag is set
   if((optionFlags & (1 << 2)) == (1 << 2)) printf("%d\t", words);

   // check if chars flag is set
   if((optionFlags & 1) == 1) printf("%d\t", chars);
   printf("%s\n", filename);

}

int main(int argc, char **argv) {

   //variables for counting lines, words, and chars.
   int lines = 0, words = 0, chars = 0;
   int totalLines = 0, totalWords = 0, totalChars = 0;

   // 8 bit flag variable for command line options
   uint8_t optionFlags = 0;

   // array of filenames
   char* filenames[argc];
   int fileCount = 0;
   for(int i = 0; i < argc; i++)
   {
      filenames[i] = NULL;
   }
   
   // parse command line input for filenames and command line options and store them in filenames and optionsFlags
   // returns the number of files given
   fileCount = command_line_parse(argc, argv, filenames, &optionFlags);

   // check if --version was passed
   // (1 << 3) == 0100
   if((optionFlags & (1 << 3)) == (1 << 3))
   {
      printf("%s version: %d\n",PROGRAM, VERSION);
      return EXIT_SUCCESS;
   }

   // if no filenames were passed, use stdin
   if(fileCount == 0)
   {
      do_wc_on_file(stdin, &lines, &words, &chars);
      print_output_with_options(lines, words, chars, " ", optionFlags);
   } else {
      // else, for each file, do wc
      for(int i = 0; i < fileCount; i++)
      {
         lines = 0;
         words = 0;
         chars = 0;
         FILE* fp;

         // if fopen returns NULL, file failed to open so error out.
         if ((fp = fopen(filenames[i], "r")) == NULL) 
         {
            fprintf(stderr, "%s: can't open [%s]\n", PROGRAM, filenames[i]);
            exit(EXIT_FAILURE);
         }
         do_wc_on_file(fp, &lines, &words, &chars);
         print_output_with_options(lines, words, chars, filenames[i], optionFlags);
         if (fileCount > 1){
            totalWords += words;
            totalLines += lines;
            totalChars += chars;
         }
         fclose(fp);
      }
   }

   if(fileCount > 1) print_output_with_options(totalLines, totalWords, totalChars, "total", optionFlags);

   // freeing filename 2d array
   for (int i = 0; i < argc; i++)
   {
      if(filenames[i] != NULL){
         free(filenames[i]);
      }
   }

   return EXIT_SUCCESS;
}
